#
#            _
#    _______| |__   ___ _ ____   __
#   |_  / __| '_ \ / _ \ '_ \ \ / /
#  _ / /\__ \ | | |  __/ | | \ V /
# (_)___|___/_| |_|\___|_| |_|\_/
#
#
# @samyak039

############
# DEFAULTS #
############
export BROWSER="firefox-developer-edition"
export EDITOR="nvim"
export MANPAGER="sh -c 'col -bx | bat -l man -p --theme default'"
export READER="zathura"
export TERMINAL="alacritty"
#export VISUAL="emacsclient -c -s doom -a 'emacs'"

############
# REQUIRED #
############
export PYENV_ROOT="$XDG_DATA_HOME/pyenv"

#######
# TEMP #
########

# zsh
export ZSH_CACHE_DIR="$XDG_CACHE_HOME/zsh"
