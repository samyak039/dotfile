(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(all-the-icons gruvbox-theme)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; LaTeX Mode
(map! (:after LaTeX
       (:map LaTeX-mode-map
        :desc "\begin{}...\end{}" "a" #'LaTeX-environment)))

;; hledger -> ledger-mode
(add-to-list 'auto-mode-alist '("\\.journal\\'" . ledger-mode))
